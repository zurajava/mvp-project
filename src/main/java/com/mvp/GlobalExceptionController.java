package com.mvp;

import com.mvp.common.ObjectMapperUtils;
import com.mvp.common.exception.AbstractValidationException;
import com.mvp.common.exception.ValidationError;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import javax.validation.ConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;


@ControllerAdvice
public class GlobalExceptionController {

  private final Logger logger = LoggerFactory.getLogger(GlobalExceptionController.class);

  @ExceptionHandler(AbstractValidationException.class)
  @ResponseBody
  public ResponseEntity<Object> handleAbstractValidationException(AbstractValidationException ex) {
    logger.debug(ex.getMessage(), ex);
    return buildResponse(ex.getErrors());
  }

  @ExceptionHandler(UsernameNotFoundException.class)
  @ResponseBody
  public ResponseEntity<Object> handleUsernameNotFoundException(UsernameNotFoundException ex) {
    return buildResponse(Arrays.asList(new ValidationError("UserName", ex.getMessage())));
  }

  @ExceptionHandler(Exception.class)
  @ResponseBody
  public ResponseEntity<Object> handleCustomException(Exception ex) {
    logger.error(ex.getMessage(), ex);
    return buildResponse(
        Arrays.asList(new ValidationError("alert", "Something unexpected happened")));
  }

  @ExceptionHandler(AccessDeniedException.class)
  public ResponseEntity<Object> accessDeniedException(Exception ex) {
    logger.error(ex.getMessage(), ex);
    return buildResponse(
        Arrays.asList(new ValidationError("alert", "Access Denied")));
  }

  @ExceptionHandler(MethodArgumentNotValidException.class)
  @ResponseBody
  public ResponseEntity<Object> handleMethodArgumentNotValidException(
      MethodArgumentNotValidException ex) {
    logger.error(ex.getMessage(), ex);

    BindingResult result = ex.getBindingResult();
    final List<ObjectError> objectErrors = result.getAllErrors();

    List<ValidationError> validationErrors = new ArrayList<>();

    for (ObjectError objectError : objectErrors) {
      validationErrors.add(new ValidationError(((FieldError) objectError).getField(),
          objectError.getDefaultMessage()));
    }
    return buildResponse(validationErrors);
  }

  @ExceptionHandler(ConstraintViolationException.class)
  @ResponseBody
  public ResponseEntity<Object> handleConstraintViolationException(
      ConstraintViolationException ex) {
    return buildResponse(
        ex.getConstraintViolations()
            .stream()
            .map(c -> new ValidationError("alert", c.getMessage()))
            .collect(Collectors.toList())
    );
  }

  private ResponseEntity<Object> buildResponse(List<ValidationError> errors) {
    final HttpHeaders httpHeaders = new HttpHeaders();
    httpHeaders.setContentType(MediaType.APPLICATION_JSON);
    return new ResponseEntity(ObjectMapperUtils.writeValueAsString(errors), httpHeaders,
        HttpStatus.BAD_REQUEST);
  }
}