package com.mvp.product;

import com.mvp.common.exception.AbstractValidationException;
import com.mvp.common.exception.ValidationError;

public class ProductException extends AbstractValidationException {

  public ProductException addError(ValidationError validationError) {
    this.errors.add(validationError);
    return this;
  }

  public static ProductException build(String field, String message) {
    return new ProductException().addError(new ValidationError(field, message));
  }
}
