package com.mvp.product;


import static com.mvp.generated.database.Tables.PRODUCT;
import static com.mvp.generated.database.Tables.USER;

import com.mvp.common.BeanUtilsWrapper;
import com.mvp.common.PageView;
import com.mvp.generated.database.tables.records.ProductRecord;
import com.mvp.product.business.AddProduct;
import com.mvp.product.business.BuyProductView;
import com.mvp.product.business.EditProduct;
import com.mvp.product.business.ProductView;
import com.mvp.product.business.SearchProduct;
import com.mvp.usermanagement.UserService;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;
import org.jooq.DSLContext;
import org.jooq.SelectConditionStep;
import org.jooq.impl.DSL;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProductServiceImpl implements ProductService {

  @Autowired
  private DSLContext dslContext;

  @Autowired
  private UserService userService;

  @Override
  public ProductView addProduct(AddProduct addProduct) throws ProductException {
    validateProductCost(addProduct.getCost());
    ProductRecord productRecord = dslContext.newRecord(PRODUCT);
    BeanUtilsWrapper.copyNonNullProperties(addProduct, productRecord);
    productRecord.insert();
    return getById(productRecord.getId(), addProduct.getSellerId());
  }

  @Override
  public ProductView editProduct(Integer id, Integer sellerId, EditProduct editProduct)
      throws ProductException {
    validateProductCost(editProduct.getCost());
    ProductRecord productRecord = getProductRecordById(id, sellerId);
    productRecord.setAmountAvailable(editProduct.getAmountAvailable());
    productRecord.setCost(editProduct.getCost());
    productRecord.setProductName(editProduct.getProductName());
    productRecord.update();

    return getById(productRecord.getId(), sellerId);
  }

  @Override
  public ProductView getById(Integer id, Integer sellerId) {
    var product = dslContext
        .select()
        .from(PRODUCT)
        .where(PRODUCT.ID.eq(id).and(PRODUCT.SELLER_ID.eq(sellerId)))
        .fetchOne();
    return product == null ? null : map(product.into(ProductRecord.class));
  }

  @Override
  public void deleteById(Integer id, Integer sellerId) {
    ProductRecord productRecord = getProductRecordById(id, sellerId);
    productRecord.delete();
  }

  @Override
  public PageView<ProductView> search(SearchProduct searchProduct) {
    PageView<ProductView> productViewPageView = new PageView<>();

    SelectConditionStep dataSelectConditionStep = buildProductSearchCondition(searchProduct);
    dataSelectConditionStep.orderBy(PRODUCT.ID.desc());

    SelectConditionStep dataCountSelectConditionStep =
        buildProductSearchCondition(searchProduct.buildWithOutLimitAndOffset());

    List<ProductRecord> users = dataSelectConditionStep.fetch().into(ProductRecord.class);
    List<ProductView> data = users.stream().map(us -> map(us))
        .collect(Collectors.toList());

    productViewPageView.setData(data);
    productViewPageView.setTotalCount(dataCountSelectConditionStep.fetch().size());

    return productViewPageView;
  }

  @Override
  public BuyProductView buyProduct(Integer userId, Integer id, Integer amountOfProduct)
      throws ProductException {
    BuyProductView buyProductView = new BuyProductView();
    //Get USer Deposit
    BigDecimal deposit = userService.getUserById(userId).getDeposit();
    ProductView product = getById(id);
    //Calculate total cost of product
    BigDecimal totalAmount = product.getCost().multiply(new BigDecimal(amountOfProduct));

    if (amountOfProduct > product.getAmountAvailable()) {
      throw ProductException.build("amount", "Amount Of Product is more then available in machine");
    }
    if (totalAmount.compareTo(deposit) > 0) {
      throw ProductException.build("amount", "Insufficient funds");
    }

    synchronized (this) {
      dslContext.update(USER).set(USER.DEPOSIT, USER.DEPOSIT.minus(totalAmount))
          .where(USER.ID.eq(userId))
          .execute();

      dslContext.update(PRODUCT)
          .set(PRODUCT.AMOUNT_AVAILABLE, PRODUCT.AMOUNT_AVAILABLE.minus(amountOfProduct))
          .where(PRODUCT.ID.eq(id))
          .execute();
    }
    buyProductView.setProduct(product);
    buyProductView.setTotalAmount(product.getCost().multiply(BigDecimal.valueOf(amountOfProduct)));
    buyProductView.setCharge(calculateCharge(userService.getUserById(userId).getDeposit()));
    return buyProductView;
  }

  @Override
  public void reset(Integer userId) {
    synchronized (this) {
      dslContext.update(USER).set(USER.DEPOSIT, new BigDecimal(0))
          .where(USER.ID.eq(userId))
          .execute();
    }
  }

  private ProductView getById(Integer id) {
    var product = dslContext
        .select()
        .from(PRODUCT)
        .where(PRODUCT.ID.eq(id))
        .fetchOne();
    return product == null ? null : map(product.into(ProductRecord.class));
  }

  private SelectConditionStep buildProductSearchCondition(SearchProduct searchProduct) {
    SelectConditionStep selectConditionStep = dslContext
        .select().from(PRODUCT).where(DSL.trueCondition());

    if (searchProduct.getSearchText() != null && !searchProduct.getSearchText().isEmpty()) {
      String likeCriteria = ("%" + searchProduct.getSearchText().trim() + "%");
      selectConditionStep.and(PRODUCT.PRODUCT_NAME.likeIgnoreCase(likeCriteria));
    }

    if (searchProduct.getSellerId() != null) {
      selectConditionStep.and(PRODUCT.SELLER_ID.eq(searchProduct.getSellerId()));

    }
    if (searchProduct.getLimit() != null) {
      selectConditionStep.limit(searchProduct.getLimit());
    }
    if (searchProduct.getOffset() != null) {
      selectConditionStep.offset(searchProduct.getOffset());
    }
    return selectConditionStep;
  }

  private ProductRecord getProductRecordById(int id) {
    return dslContext
        .select().from(PRODUCT).where(PRODUCT.ID.eq(id)).fetchAny().into(ProductRecord.class);
  }

  private ProductRecord getProductRecordById(int id, int sellerId) {
    return dslContext
        .select().from(PRODUCT).where(PRODUCT.ID.eq(id).and(PRODUCT.SELLER_ID.eq(sellerId)))
        .fetchAny().into(ProductRecord.class);
  }

  private ProductView map(ProductRecord productRecord) {
    var productView = new ProductView();
    BeanUtils.copyProperties(productRecord, productView);
    return productView;
  }

  private void validateProductCost(BigDecimal cost) throws ProductException {
    if (cost.intValue() % 5 != 0) {
      throw ProductException.build("cost", "Cost should be in multiples of 5");
    }
  }

  private HashMap<Integer, Integer> calculateCharge(BigDecimal charge) {
    HashMap<Integer, Integer> map = new HashMap<Integer, Integer>();
    division(charge, map);
    return map;
  }

  public static void division(BigDecimal charge, HashMap<Integer, Integer> map) {
    Integer count100 = charge.divide(new BigDecimal(100), 0, RoundingMode.DOWN).intValue();
    map.put(100, count100);

    Integer count50 = (charge.subtract(new BigDecimal(count100 * 100))).divide(new BigDecimal(50),
        0, RoundingMode.DOWN).intValue();
    map.put(50, count50);

    Integer count20 = (charge.subtract(new BigDecimal((count100 * 100) + (count50 * 50)))).divide(
        new BigDecimal(20),
        0, RoundingMode.DOWN).intValue();
    map.put(20, count20);

    Integer count10 = (charge.subtract(
        new BigDecimal((count100 * 100) + (count50 * 50) + (count20 * 20)))).divide(
        new BigDecimal(10),
        0, RoundingMode.DOWN).intValue();
    map.put(10, count10);

    Integer count5 = (charge.subtract(
        new BigDecimal(
            (count100 * 100) + (count50 * 50) + (count20 * 20) + (count10 * 10)))).divide(
        new BigDecimal(5),
        0, RoundingMode.DOWN).intValue();
    map.put(5, count5);
  }
}
