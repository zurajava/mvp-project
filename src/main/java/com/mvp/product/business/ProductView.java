package com.mvp.product.business;

import java.math.BigDecimal;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProductView {

  private Integer id;
  private Integer amountAvailable;
  private BigDecimal cost;
  private String productName;
  private Integer sellerId;
  private Boolean active;
}
