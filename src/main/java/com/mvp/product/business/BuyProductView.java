package com.mvp.product.business;

import java.math.BigDecimal;
import java.util.HashMap;
import lombok.Getter;
import lombok.Setter;
import org.springframework.web.bind.annotation.GetMapping;

@Getter
@Setter
public class BuyProductView {

  private BigDecimal totalAmount;
  private ProductView product;
  private HashMap<Integer, Integer> charge;
}
