package com.mvp.product.business;

import com.mvp.generated.database.enums.UserRole;
import com.mvp.usermanagement.busuness.SearchUser;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SearchProduct {

  private String searchText;
  private Integer sellerId;
  private Integer limit = 20;
  private Integer offset = 0;

  public SearchProduct buildWithOutLimitAndOffset() {
    SearchProduct searchProduct = new SearchProduct();
    searchProduct.setSearchText(searchText);
    searchProduct.setSellerId(sellerId);
    searchProduct.setLimit(null);
    searchProduct.setOffset(null);
    return searchProduct;
  }
}
