package com.mvp.product.business;

import java.math.BigDecimal;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EditProduct {

  private Integer amountAvailable;
  private BigDecimal cost;
  private String productName;
}
