package com.mvp.product;

import com.mvp.common.PageView;
import com.mvp.product.business.AddProduct;
import com.mvp.product.business.BuyProductView;
import com.mvp.product.business.EditProduct;
import com.mvp.product.business.ProductView;
import com.mvp.product.business.SearchProduct;

public interface ProductService {

  ProductView addProduct(AddProduct addProduct) throws ProductException;

  ProductView editProduct(Integer id, Integer sellerId, EditProduct EditProduct)
      throws ProductException;

  ProductView getById(Integer id, Integer sellerId);

  void deleteById(Integer id, Integer sellerId);

  PageView<ProductView> search(SearchProduct searchProduct);

  BuyProductView buyProduct(Integer userId, Integer id, Integer amountOfProduct) throws ProductException ;

  void reset(Integer userId);

}
