package com.mvp.product;

import com.mvp.common.PageView;
import com.mvp.product.business.AddProduct;
import com.mvp.product.business.BuyProductView;
import com.mvp.product.business.EditProduct;
import com.mvp.product.business.ProductView;
import com.mvp.product.business.SearchProduct;
import com.mvp.usermanagement.UserContextController;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ProductController extends UserContextController {

  @Autowired
  private ProductService productService;

  @PostMapping(value = "/product")
  @PreAuthorize("hasAnyAuthority('seller')")
  public ProductView addProduct(@RequestBody @Valid AddProduct addProduct)
      throws ProductException {
    if (isCurrentUserSeller()) {
      addProduct.setSellerId(getCurrentUserId());
    }
    return productService.addProduct(addProduct);
  }

  @PutMapping(value = "/product/{id}")
  @PreAuthorize("hasAnyAuthority('seller')")
  public ProductView editProduct(@RequestBody @Valid EditProduct editProduct,
      @PathVariable Integer id) throws ProductException {
    return productService.editProduct(id, getCurrentUserId(), editProduct);
  }

  @GetMapping(value = "/product/{id}")
  @PreAuthorize("hasAnyAuthority('seller')")
  public ProductView getById(@PathVariable Integer id) {
    return productService.getById(id, getCurrentUserId());
  }

  @DeleteMapping(value = "/product/{id}")
  @PreAuthorize("hasAnyAuthority('seller')")
  public void deleteById(@PathVariable Integer id) {
    productService.deleteById(id, getCurrentUserId());
  }

  //Edge case search product
  @GetMapping(value = "/product")
  public PageView<ProductView> search(@RequestBody @Valid SearchProduct searchProduct) {
    return productService.search(searchProduct);
  }

  @PostMapping(value = "/buy/{id}")
  @PreAuthorize("hasAnyAuthority('buyer')")
  public BuyProductView buyProduct(@PathVariable Integer id,
      @RequestParam Integer amountOfProduct) throws ProductException {
    return productService.buyProduct(getCurrentUserId(), id, amountOfProduct);
  }

  @PostMapping(value = "/reset")
  @PreAuthorize("hasAnyAuthority('buyer')")
  public void reset() {
    productService.reset(getCurrentUserId());
  }

}
