/*
 * This file is generated by jOOQ.
 */
package com.mvp.generated.database;


import com.mvp.generated.database.tables.Product;
import com.mvp.generated.database.tables.User;


/**
 * Convenience access to all tables in public
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Tables {

    /**
     * The table <code>public.product</code>.
     */
    public static final Product PRODUCT = Product.PRODUCT;

    /**
     * The table <code>public.user</code>.
     */
    public static final User USER = User.USER;
}
