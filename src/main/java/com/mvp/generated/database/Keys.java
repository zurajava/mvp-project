/*
 * This file is generated by jOOQ.
 */
package com.mvp.generated.database;


import com.mvp.generated.database.tables.Product;
import com.mvp.generated.database.tables.User;
import com.mvp.generated.database.tables.records.ProductRecord;
import com.mvp.generated.database.tables.records.UserRecord;

import org.jooq.ForeignKey;
import org.jooq.Identity;
import org.jooq.TableField;
import org.jooq.UniqueKey;
import org.jooq.impl.Internal;


/**
 * A class modelling foreign key relationships and constraints of tables of 
 * the <code>public</code> schema.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Keys {

    // -------------------------------------------------------------------------
    // IDENTITY definitions
    // -------------------------------------------------------------------------

    public static final Identity<ProductRecord, Integer> IDENTITY_PRODUCT = Identities0.IDENTITY_PRODUCT;
    public static final Identity<UserRecord, Integer> IDENTITY_USER = Identities0.IDENTITY_USER;

    // -------------------------------------------------------------------------
    // UNIQUE and PRIMARY KEY definitions
    // -------------------------------------------------------------------------

    public static final UniqueKey<ProductRecord> PRODUCT_PKEY = UniqueKeys0.PRODUCT_PKEY;
    public static final UniqueKey<UserRecord> USER_PKEY = UniqueKeys0.USER_PKEY;
    public static final UniqueKey<UserRecord> USER_USERNAME_KEY = UniqueKeys0.USER_USERNAME_KEY;

    // -------------------------------------------------------------------------
    // FOREIGN KEY definitions
    // -------------------------------------------------------------------------

    public static final ForeignKey<ProductRecord, UserRecord> PRODUCT__PRODUCT_SELLER_ID_FKEY = ForeignKeys0.PRODUCT__PRODUCT_SELLER_ID_FKEY;

    // -------------------------------------------------------------------------
    // [#1459] distribute members to avoid static initialisers > 64kb
    // -------------------------------------------------------------------------

    private static class Identities0 {
        public static Identity<ProductRecord, Integer> IDENTITY_PRODUCT = Internal.createIdentity(Product.PRODUCT, Product.PRODUCT.ID);
        public static Identity<UserRecord, Integer> IDENTITY_USER = Internal.createIdentity(User.USER, User.USER.ID);
    }

    private static class UniqueKeys0 {
        public static final UniqueKey<ProductRecord> PRODUCT_PKEY = Internal.createUniqueKey(Product.PRODUCT, "product_pkey", new TableField[] { Product.PRODUCT.ID }, true);
        public static final UniqueKey<UserRecord> USER_PKEY = Internal.createUniqueKey(User.USER, "user_pkey", new TableField[] { User.USER.ID }, true);
        public static final UniqueKey<UserRecord> USER_USERNAME_KEY = Internal.createUniqueKey(User.USER, "user_username_key", new TableField[] { User.USER.USERNAME }, true);
    }

    private static class ForeignKeys0 {
        public static final ForeignKey<ProductRecord, UserRecord> PRODUCT__PRODUCT_SELLER_ID_FKEY = Internal.createForeignKey(Keys.USER_PKEY, Product.PRODUCT, "product_seller_id_fkey", new TableField[] { Product.PRODUCT.SELLER_ID }, true);
    }
}
