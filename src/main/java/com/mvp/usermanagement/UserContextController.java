package com.mvp.usermanagement;

import com.mvp.generated.database.enums.UserRole;
import com.mvp.security.jwt.JwtUser;
import com.mvp.usermanagement.busuness.UserView;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserContextController {

  public String getCurrentUserGroup() {
    if (isCurrentUserSeller()) {
      return UserRole.seller.getLiteral();
    } else if (isCurrentUserBuyer()) {
      return UserRole.buyer.getLiteral();
    }
    return null;
  }

  public Integer getCurrentUserId() {
    return JwtUser.getInstance().getUserView().getId();
  }

  public UserView getActionUser() {
    UserView user = JwtUser.getInstance().getUserView();
    return user;
  }

  public boolean isCurrentUserSeller() {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    return auth.getAuthorities()
        .contains(new SimpleGrantedAuthority(UserRole.seller.getLiteral()));
  }

  public boolean isCurrentUserBuyer() {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    return auth.getAuthorities()
        .contains(new SimpleGrantedAuthority(UserRole.buyer.getLiteral()));
  }

  public boolean isUserAuth() {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    return !auth.getPrincipal().equals("anonymousUser");
  }
}
