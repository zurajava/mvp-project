package com.mvp.usermanagement;

import com.mvp.common.PageView;
import com.mvp.usermanagement.busuness.AddUser;
import com.mvp.usermanagement.busuness.EditUser;
import com.mvp.usermanagement.busuness.SearchUser;
import com.mvp.usermanagement.busuness.UserView;
import java.util.Optional;

public interface UserService {

  Optional<UserView> getUser(String username, String password);

  UserView getUserById(int userId);

  boolean verifyPassword(int userId, String password);

  Optional<UserView> getUserByUserName(String userName);

  UserView addUser(AddUser addUser) throws UserAlreadyExistsException;

  UserView updateUser(int id, EditUser editUser) throws UserAlreadyExistsException;

  UserView getById(int id);

  void deleteById(int id);

  PageView<UserView> search(SearchUser searchUser);

  void deposit(Integer id, Integer coin) throws CoinValidatorException;

}
