package com.mvp.usermanagement.busuness;

import com.mvp.generated.database.enums.UserRole;
import java.math.BigDecimal;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserView {

  private Integer id;
  private String username;
  private String password;
  private BigDecimal deposit;
  private UserRole role;
}
