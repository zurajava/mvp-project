package com.mvp.usermanagement.busuness;


import com.mvp.generated.database.enums.UserRole;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SearchUser {

  private String searchText;
  private UserRole role;
  private Integer limit = 20;
  private Integer offset = 0;

  public SearchUser buildWithOutLimitAndOffset() {
    SearchUser searchUser = new SearchUser();
    searchUser.setSearchText(searchText);
    searchUser.setRole(role);
    searchUser.setLimit(null);
    searchUser.setOffset(null);
    return searchUser;
  }
}
