package com.mvp.usermanagement;

import com.mvp.auth.AuthenticationController;
import com.mvp.common.PageView;
import com.mvp.usermanagement.busuness.AddUser;
import com.mvp.usermanagement.busuness.EditUser;
import com.mvp.usermanagement.busuness.SearchUser;
import com.mvp.usermanagement.busuness.UserView;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController extends UserContextController {

  @Autowired
  private UserService userService;
  @Autowired
  private AuthenticationController authenticationController;

  @PostMapping(value = "/user")
  public UserView addUser(@RequestBody @Valid AddUser addUser) throws Exception {
    return userService.addUser(addUser);
  }

  @PutMapping(value = "/user/{id}")
  public UserView updateUser(@RequestBody @Valid EditUser editUser, @PathVariable Integer id,
      HttpServletResponse httpServletResponse) throws Exception {
    UserView userView = userService.updateUser(id, editUser);
    authenticationController.refreshAuthenticatedUser(httpServletResponse);
    return userView;
  }

  @GetMapping(value = "/user/{id}")
  public UserView getById(@PathVariable Integer id) {
    UserView userView = userService.getById(id);
    return userView;
  }

  @DeleteMapping(value = "/user/{id}")
  public void deleteById(@PathVariable Integer id) {
    userService.deleteById(id);
  }

  //Edge case search user
  @GetMapping("/user")
  public PageView<UserView> search(@RequestBody SearchUser search) {
    PageView<UserView> userViewPageView = userService.search(search);
    return userViewPageView;
  }

  @PutMapping(value = "/deposit")
  @PreAuthorize("hasAnyAuthority('buyer')")
  public void deposit(@RequestParam Integer coin) throws Exception {
    userService.deposit(getCurrentUserId(), coin);
  }
}
