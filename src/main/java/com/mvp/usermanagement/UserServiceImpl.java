package com.mvp.usermanagement;

import com.mvp.common.BeanUtilsWrapper;
import com.mvp.common.PageView;
import com.mvp.common.hashing.Argon2Utils;
import com.mvp.generated.database.Tables;
import com.mvp.generated.database.tables.records.UserRecord;
import com.mvp.usermanagement.busuness.AddUser;
import com.mvp.usermanagement.busuness.EditUser;
import com.mvp.usermanagement.busuness.SearchUser;
import com.mvp.usermanagement.busuness.UserView;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;
import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.SelectConditionStep;
import org.jooq.impl.DSL;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static com.mvp.generated.database.Tables.USER;

@Service
public class UserServiceImpl implements UserService {

  @Autowired
  private DSLContext dslContext;
  private static final HashMap<Integer, Integer> coins;

  static {
    coins = new HashMap<>();
    coins.put(5, 5);
    coins.put(10, 10);
    coins.put(20, 20);
    coins.put(50, 50);
    coins.put(100, 100);
  }


  @Override
  public Optional<UserView> getUser(String username, String password) {
    Record record =
        dslContext
            .select()
            .from(Tables.USER)
            .where(Tables.USER.USERNAME.eq(username))
            .fetchAny();
    if (record != null) {
      UserRecord userRecord = record.into(UserRecord.class);
      if (Argon2Utils.verify(userRecord.getPassword(), password)) {
        return Optional.of(userRecord.into(UserView.class));
      }
    }
    return Optional.empty();
  }

  @Override
  public UserView getUserById(int userId) {
    var user = dslContext
        .select()
        .from(Tables.USER)
        .where(Tables.USER.ID.eq(userId))
        .fetchOne();
    return user == null ? null : map(user.into(UserRecord.class));
  }

  @Override
  public boolean verifyPassword(int userId, String password) {
    UserRecord userRecord = getUserRecordById(userId);
    return Argon2Utils.verify(userRecord.getPassword(), password);
  }

  @Override
  public Optional<UserView> getUserByUserName(String userName) {
    Record record =
        dslContext
            .select()
            .from(Tables.USER)
            .where(Tables.USER.USERNAME.equalIgnoreCase(userName))
            .fetchAny();
    return record == null ? Optional.empty() : Optional.of(map(record.into(UserRecord.class)));

  }

  @Override
  public UserView addUser(AddUser addUser) throws UserAlreadyExistsException {

    validateUser(addUser.getUsername());

    UserRecord userRecord = dslContext.newRecord(USER);
    BeanUtilsWrapper.copyNonNullProperties(addUser, userRecord);
    userRecord.setPassword(Argon2Utils.hash(addUser.getPassword()));
    userRecord.insert();
    return getUserById(userRecord.getId());
  }

  @Override
  public UserView updateUser(int id, EditUser editUser) throws UserAlreadyExistsException {
    validateUser(editUser.getUsername());
    UserRecord uRecord = getUserRecordById(id);
    uRecord.setUsername(editUser.getUsername());
    uRecord.setDeposit(editUser.getDeposit());
    uRecord.setPassword(Argon2Utils.hash(editUser.getPassword()));
    uRecord.setRole(editUser.getRole());
    uRecord.update();

    return getUserById(uRecord.getId());
  }

  @Override
  public UserView getById(int id) {
    UserRecord uRecord = getUserRecordById(id);
    return map(uRecord);
  }

  @Override
  public void deleteById(int id) {
    UserRecord uRecord = getUserRecordById(id);
    uRecord.delete();
  }

  @Override
  public PageView<UserView> search(SearchUser searchUser) {
    PageView<UserView> userViewPageView = new PageView<>();

    SelectConditionStep dataSelectConditionStep = buildUserSearchCondition(searchUser);
    dataSelectConditionStep.orderBy(USER.ID.desc());
    SelectConditionStep dataCountSelectConditionStep =
        buildUserSearchCondition(searchUser.buildWithOutLimitAndOffset());

    List<UserRecord> users = dataSelectConditionStep.fetch().into(UserRecord.class);
    List<UserView> data = users.stream().map(us -> map(us))
        .collect(Collectors.toList());

    userViewPageView.setData(data);
    userViewPageView.setTotalCount(dataCountSelectConditionStep.fetch().size());

    return userViewPageView;
  }

  @Override
  public void deposit(Integer id, Integer coin) throws CoinValidatorException {
    if (!coins.containsKey(coin)) {
      throw CoinValidatorException.build("Coin",
          "Unknown coin, it must be in 5, 10, 20, 50 and 100");
    }
    synchronized (this) {
      dslContext.update(USER).set(USER.DEPOSIT, USER.DEPOSIT.plus(coin))
          .where(USER.ID.eq(id))
          .execute();
    }
  }

  private SelectConditionStep buildUserSearchCondition(SearchUser searchUser) {
    SelectConditionStep selectConditionStep = dslContext
        .select().from(USER).where(DSL.trueCondition());

    if (searchUser.getSearchText() != null && !searchUser.getSearchText().isEmpty()) {
      String likeCriteria = ("%" + searchUser.getSearchText().trim() + "%");
      selectConditionStep.and(USER.USERNAME.likeIgnoreCase(likeCriteria));
    }

    if (searchUser.getRole() != null) {
      selectConditionStep.and(USER.ROLE.eq(searchUser.getRole()));

    }
    if (searchUser.getLimit() != null) {
      selectConditionStep.limit(searchUser.getLimit());
    }
    if (searchUser.getOffset() != null) {
      selectConditionStep.offset(searchUser.getOffset());
    }
    return selectConditionStep;
  }

  private UserRecord getUserRecordById(int userId) {
    return dslContext
        .select().from(USER).where(USER.ID.eq(userId)).fetchAny().into(UserRecord.class);
  }

  private Optional<UserView> getUserByUserNameIfExists(String userName) {
    Record record = dslContext.select().from(USER).where(USER.USERNAME.equalIgnoreCase(userName))
        .fetchAny();
    return record == null ? Optional.empty() : Optional.of(map(record.into(UserRecord.class)));
  }

  private void validateUser(String userName)
      throws UserAlreadyExistsException {
    Optional<UserView> withEmail = getUserByUserNameIfExists(userName);
    if (withEmail.isPresent()) {
      String message = String
          .format("User with this user name '%s' is already registered", userName);
      throw UserAlreadyExistsException.build("userName", message);
    }
  }

  private UserView map(UserRecord userRecord) {
    var userView = new UserView();
    BeanUtils.copyProperties(userRecord, userView);
    return userView;
  }
}
