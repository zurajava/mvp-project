package com.mvp.usermanagement;

import com.mvp.common.exception.AbstractValidationException;
import com.mvp.common.exception.ValidationError;

public class CoinValidatorException extends AbstractValidationException {

  public CoinValidatorException addError(ValidationError validationError) {
    this.errors.add(validationError);
    return this;
  }

  public static CoinValidatorException build(String field, String message) {
    return new CoinValidatorException().addError(new ValidationError(field, message));
  }
}
