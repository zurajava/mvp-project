package com.mvp.auth;

import com.google.gson.Gson;
import com.mvp.security.jwt.JwtRequest;
import com.mvp.security.jwt.JwtTokenUtil;
import com.mvp.security.jwt.JwtUser;
import com.mvp.security.jwt.JwtUserDetailsService;
import com.mvp.security.jwt.tokenproviders.TokenCookieProvider;
import com.mvp.usermanagement.UserService;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AuthenticationController {

  private final Logger LOGGER = LoggerFactory.getLogger(AuthenticationController.class);
  public static final String authenticatedUser = "authenticatedUser";

  @Autowired
  private AuthenticationManager authenticationManager;
  @Autowired
  private JwtTokenUtil jwtTokenUtil;
  @Autowired
  private JwtUserDetailsService userDetailsService;
  @Autowired
  private UserService userService;

  @PostMapping(value = "/sign-in")
  public String signIn(@RequestBody JwtRequest authenticationRequest,
      HttpServletRequest httpServletRequest,
      HttpServletResponse httpServletResponse)
      throws Exception {
    authenticate(authenticationRequest.getUsername(), authenticationRequest.getPassword());

    final JwtUser jwtUser = (JwtUser) userDetailsService.loadUserByUsername(
        authenticationRequest.getUsername());
    return setCookie(jwtUser, httpServletResponse);
  }

  @PostMapping(value = "/sign-out")
  public void signOut(HttpServletResponse httpServletResponse) {
    Cookie cookie = new Cookie(TokenCookieProvider.JWT_TOKEN, null);
    cookie.setHttpOnly(true);
    httpServletResponse.addCookie(cookie);
  }

  private void authenticate(String username, String password) throws Exception {
    try {
      authenticationManager
          .authenticate(new UsernamePasswordAuthenticationToken(username, password));
    } catch (DisabledException | BadCredentialsException | LockedException e) {
      throw new SimpleValidationException("Invalid Credentials");
    }
  }

  public void refreshAuthenticatedUser(HttpServletResponse httpServletResponse) {
    JwtUser jwtUser = JwtUser.getInstance();
    jwtUser.setUserView(userService.getUserById(jwtUser.getUserView().getId()));
    setCookie(jwtUser, httpServletResponse);
  }

  //Set token in cookie for testing purpose from postman
  private String setCookie(JwtUser jwtUser, HttpServletResponse httpServletResponse) {
    Map<String, Object> claims = new HashMap<>();
    claims.put(authenticatedUser, new Gson().toJson(jwtUser));
    final String token = jwtTokenUtil.generateToken(jwtUser.getUsername(), claims);
    Cookie cookie = new Cookie(TokenCookieProvider.JWT_TOKEN, token);
    cookie.setHttpOnly(true);
    cookie.setMaxAge(60 * 60 * 24 * 7);
    cookie.setPath("/");
    httpServletResponse.addCookie(cookie);
    jwtUser.setToken(token);
    return token;
  }
}