package com.mvp.auth;

import com.mvp.common.exception.AbstractValidationException;
import com.mvp.common.exception.ValidationError;

public class SimpleValidationException extends AbstractValidationException {

  public SimpleValidationException(String message) {
    addError(new ValidationError("alert", message));
  }
}
