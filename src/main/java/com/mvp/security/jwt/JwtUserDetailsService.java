package com.mvp.security.jwt;

import com.mvp.usermanagement.UserService;
import com.mvp.usermanagement.busuness.UserView;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class JwtUserDetailsService implements UserDetailsService {

  @Autowired
  private UserService userService;

  @Override
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    JwtUser jwtUser = new JwtUser();
    Optional<UserView> accountViewOptional = userService.getUserByUserName(username);
    if (accountViewOptional.isPresent()) {
      jwtUser.setUserView(accountViewOptional.get());
      return jwtUser;
    } else {
      throw new UsernameNotFoundException("User not found with username: " + username);
    }
  }
}