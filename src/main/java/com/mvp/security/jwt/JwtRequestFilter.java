package com.mvp.security.jwt;

import com.google.gson.Gson;
import com.mvp.security.jwt.tokenproviders.TokenProvider;
import java.io.IOException;
import java.util.List;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

@Component
public class JwtRequestFilter extends OncePerRequestFilter {

  public static final String authenticatedUser = "authenticatedUser";
  @Autowired
  private JwtUserDetailsService jwtUserDetailsService;
  @Autowired
  private List<TokenProvider> tokenProviders;
  @Autowired
  private JwtTokenUtil jwtTokenUtil;
  @Value("${security.publicResources}")
  private String[] publicResources;
  private final Logger LOGGER = LoggerFactory.getLogger(JwtRequestFilter.class);

  @Override
  protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response,
      FilterChain chain)
      throws ServletException, IOException {
    String username = null;
    String jwtToken = null;
    JwtUser jwtUser = null;

    //ignore public resources
    for (String resource : publicResources) {
      if (new AntPathRequestMatcher(resource).matches(request)) {
        chain.doFilter(request, response);
        return;
      }
    }
    for (TokenProvider tokenProvider : tokenProviders) {
      jwtToken = tokenProvider.getToken(request);
      if (jwtToken != null) {
        break;
      }
    }
    try {
      username = jwtTokenUtil.getUsernameFromToken(jwtToken);
      Object serializedIUser = jwtTokenUtil.getClaimFromToken(jwtToken,
          claims -> claims.get(authenticatedUser));
      jwtUser = new Gson().fromJson(serializedIUser.toString(), JwtUser.class);
    } catch (Exception e) {
      LOGGER.trace(String.format(
          "Unable to get JWT Token : %s for username %s} ", jwtToken, username, e));
    }
    if (jwtUser != null && SecurityContextHolder.getContext().getAuthentication() == null) {
      UserDetails dbUserDetails;
      try {
        dbUserDetails = jwtUserDetailsService.loadUserByUsername(jwtUser.getUsername());
        if (jwtTokenUtil.validateToken(jwtToken, dbUserDetails)) {
          UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(
              jwtUser, null, jwtUser.getAuthorities());
          usernamePasswordAuthenticationToken
              .setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
          SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
          chain.doFilter(request, response);
          return;
        }
      } catch (UsernameNotFoundException ex) {
        LOGGER.trace(String.format(
            "UserName not found : %s for username %s ", jwtToken, username));
      }
    }
    response.sendError(401);
    return;
  }
}