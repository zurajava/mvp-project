package com.mvp.security.jwt;

import com.mvp.usermanagement.UserService;
import com.mvp.usermanagement.busuness.UserView;
import java.util.Arrays;
import java.util.Optional;
import java.util.stream.Collectors;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

@Component
public class JwtDatabaseAuthenticationProvider implements AuthenticationProvider {

  private final UserService userService;

  public JwtDatabaseAuthenticationProvider(UserService userService) {
    this.userService = userService;
  }


  @Override
  public boolean supports(Class<?> authentication) {
    return authentication.equals(UsernamePasswordAuthenticationToken.class);
  }

  @Override
  public Authentication authenticate(Authentication authentication) throws AuthenticationException {
    String userName = authentication.getName();
    String password = authentication.getCredentials().toString();
    Optional<UserView> accountViewOptional = userService.getUserByUserName(userName);
    if (!accountViewOptional.isPresent()) {
      throw new BadCredentialsException("Authentication failed");
    }
    UserView accountView = accountViewOptional.get();
    if (!userService.verifyPassword(accountView.getId(), password)) {
      throw new BadCredentialsException("Authentication failed");
    }

    return new UsernamePasswordAuthenticationToken(userName, null,
        Arrays.asList(new SimpleGrantedAuthority(accountView.getRole().getLiteral())));
  }
}
