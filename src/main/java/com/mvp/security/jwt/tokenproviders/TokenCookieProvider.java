package com.mvp.security.jwt.tokenproviders;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Component;

@Component
public class TokenCookieProvider implements TokenProvider {

  public static final String JWT_TOKEN = "X-TOKEN";

  @Override
  public String getToken(HttpServletRequest request) {
    Cookie[] cookies = request.getCookies();
    if (cookies != null && cookies.length > 0) {
      for (Cookie cookie : cookies) {
        if (cookie.getName().equals(JWT_TOKEN) && cookie.getValue() != null) {
          return cookie.getValue();
        }
      }
    }
    return null;
  }
}
