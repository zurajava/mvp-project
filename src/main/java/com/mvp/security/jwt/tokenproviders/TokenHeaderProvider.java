package com.mvp.security.jwt.tokenproviders;

import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Component;

@Component
public class TokenHeaderProvider implements TokenProvider {

  @Override
  public String getToken(HttpServletRequest request) {
    final String requestTokenHeader = request.getHeader("Authorization");
    if (requestTokenHeader != null && requestTokenHeader.startsWith("Bearer ")) {
      return requestTokenHeader.substring(7);
    }
    return null;
  }
}
