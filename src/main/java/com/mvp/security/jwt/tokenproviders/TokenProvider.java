package com.mvp.security.jwt.tokenproviders;

import javax.servlet.http.HttpServletRequest;

public interface TokenProvider {

  String getToken(HttpServletRequest data);
}
