package com.mvp.common.hashing;

import de.mkammerer.argon2.Argon2;
import de.mkammerer.argon2.Argon2Factory;

public class Argon2Utils {

  public static String hash(String text) {
    Argon2 argon2 = Argon2Factory.create();
    String hash = argon2.hash(2, 65536, 1, text.toCharArray());
    return hash;
  }

  public static boolean verify(String hash, String password) {
    if (password == null || hash == null) {
      return false;
    }
    Argon2 argon2 = Argon2Factory.create();
    return argon2.verify(hash, password);
  }
}
