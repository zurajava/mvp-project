package com.mvp.common;

import java.util.List;


public class PageView<T> {

  private Integer totalCount;
  private List<T> data;

  public PageView(List<T> data, Integer totalCount) {
    this.totalCount = totalCount;
    this.data = data;
  }

  public PageView() {
  }

  public Integer getTotalCount() {
    return totalCount;
  }

  public void setTotalCount(Integer totalCount) {
    this.totalCount = totalCount;
  }

  public List<T> getData() {
    return data;
  }

  public PageView setData(List<T> data) {
    this.data = data;
    return this;
  }
}
