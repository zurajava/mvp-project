package com.mvp.acceptance;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.mvp.common.exception.ValidationError;
import com.mvp.product.business.BuyProductView;
import com.mvp.security.jwt.JwtRequest;
import java.util.ArrayList;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

/*
   Acceptance tests for sign-in,deposit and buy method
 */
public class AuthControllerTest extends AbstractTest {

  @Override
  @Before
  public void setUp() {
    super.setUp();
  }

  @Test
  public void loginAndGetToken_success() throws Exception {
    String uri = "/sign-in";

    JwtRequest request = new JwtRequest();
    request.setUsername("zurab");
    request.setPassword("zurab");
    String json = mapToJson(request);

    MvcResult mvcResult = mvc.perform(
        MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON)
            .content(json)).andReturn();
    String content = mvcResult.getResponse().getContentAsString();
    assertEquals(HttpStatus.OK.value(), mvcResult.getResponse().getStatus());
    assertTrue(content != null);
  }

  @Test
  public void loginUserNameOrPassword_fail() throws Exception {
    String uri = "/sign-in";

    JwtRequest request = new JwtRequest();
    request.setUsername("zurab1");
    request.setPassword("zurab");
    String json = mapToJson(request);

    MvcResult mvcResult = mvc.perform(
        MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON)
            .content(json)).andReturn();
    assertEquals(HttpStatus.BAD_REQUEST.value(), mvcResult.getResponse().getStatus());
    String content = mvcResult.getResponse().getContentAsString();
    ArrayList<ValidationError> res = new Gson().fromJson(content,
        new TypeToken<ArrayList<ValidationError>>() {
        }.getType());
    assertTrue(res.size() == 1);
    assertTrue(res.get(0).getField().equals("alert"));
    assertTrue(res.get(0).getMessage().equals("Invalid Credentials"));
  }

  @Test
  public void depositTest_success() throws Exception {
    String uri = "/deposit";

    JwtRequest request = new JwtRequest();
    request.setUsername("zurab");
    request.setPassword("zurab");
    String json = mapToJson(request);
    String token = getToken(json);

    MvcResult mvcResult = mvc.perform(
        MockMvcRequestBuilders.put(uri).param("coin", String.valueOf(5))
            .header("Authorization", "Bearer " + token)).andReturn();
    assertEquals(HttpStatus.OK.value(), mvcResult.getResponse().getStatus());
  }

  @Test
  public void depositIncorrectCoinTest_fail() throws Exception {
    String uri = "/deposit";

    JwtRequest request = new JwtRequest();
    request.setUsername("zurab");
    request.setPassword("zurab");
    String json = mapToJson(request);
    String token = getToken(json);

    MvcResult mvcResult = mvc.perform(
        MockMvcRequestBuilders.put(uri).param("coin", String.valueOf(7))
            .header("Authorization", "Bearer " + token)).andReturn();
    ArrayList<ValidationError> res = new Gson().fromJson(
        mvcResult.getResponse().getContentAsString(),
        new TypeToken<ArrayList<ValidationError>>() {
        }.getType());

    assertEquals(HttpStatus.BAD_REQUEST.value(), mvcResult.getResponse().getStatus());
    assertTrue(res.get(0).getField().equals("Coin"));
    assertTrue(res.get(0).getMessage().equals("Unknown coin, it must be in 5, 10, 20, 50 and 100"));
  }

  @Test
  public void depositIncorrectUserRoleTest_fail() throws Exception {
    String uri = "/deposit";

    JwtRequest request = new JwtRequest();
    request.setUsername("zuras");
    request.setPassword("zuras");
    String json = mapToJson(request);
    String token = getToken(json);

    MvcResult mvcResult = mvc.perform(
        MockMvcRequestBuilders.put(uri).param("coin", String.valueOf(5))
            .header("Authorization", "Bearer " + token)).andReturn();
    ArrayList<ValidationError> res = new Gson().fromJson(
        mvcResult.getResponse().getContentAsString(),
        new TypeToken<ArrayList<ValidationError>>() {
        }.getType());

    assertEquals(HttpStatus.BAD_REQUEST.value(), mvcResult.getResponse().getStatus());
    assertTrue(res.get(0).getField().equals("alert"));
    assertTrue(res.get(0).getMessage().equals("Access Denied"));
  }

  @Test
  public void buyTest_success() throws Exception {
    String uri = "/buy/10";

    JwtRequest request = new JwtRequest();
    request.setUsername("zurab");
    request.setPassword("zurab");
    String json = mapToJson(request);
    String token = getToken(json);

    MvcResult mvcResult = mvc.perform(
        MockMvcRequestBuilders.post(uri).param("amountOfProduct", String.valueOf(1))
            .header("Authorization", "Bearer " + token)).andReturn();

    BuyProductView res = mapFromJson(mvcResult.getResponse().getContentAsString(),
        BuyProductView.class);

    assertEquals(HttpStatus.OK.value(), mvcResult.getResponse().getStatus());
    assertTrue(res != null);

  }
}
