package com.mvp.usermanagement;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.google.gson.Gson;
import com.mvp.auth.AuthenticationController;
import com.mvp.generated.database.enums.UserRole;
import com.mvp.usermanagement.busuness.AddUser;
import com.mvp.usermanagement.busuness.EditUser;
import com.mvp.usermanagement.busuness.UserView;
import java.math.BigDecimal;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;


@SpringBootTest
@AutoConfigureMockMvc(addFilters = false)
public class UserControllerControllerTest {

  @Autowired
  protected MockMvc mockMvc;
  @MockBean
  private UserService service;
  @MockBean
  private AuthenticationController authenticationController;


  @Test
  public void registerNewUser__success() throws Exception {
    AddUser addUser = new AddUser();
    addUser.setUsername("test");
    addUser.setPassword("test");
    addUser.setDeposit(new BigDecimal(0));
    addUser.setRole(UserRole.seller);

    UserView userView = new UserView();
    userView.setId(1);
    userView.setUsername("test");
    userView.setPassword("test");
    userView.setDeposit(new BigDecimal(0));
    userView.setRole(UserRole.seller);

    given(service.addUser(addUser)).willReturn(userView);

    String uri = "/user";
    this.mockMvc.perform(post(uri).contentType(MediaType.APPLICATION_JSON)
            .content(getJsonFromPojo(addUser)))
        .andDo(print())
        .andExpect(status().isOk());

  }

  //Test is not finished
  @Test
  public void updateNewUser__success() throws Exception {
    EditUser updateUser = new EditUser();
    updateUser.setUsername("test");
    updateUser.setPassword("test_update");
    updateUser.setDeposit(new BigDecimal(0));
    updateUser.setRole(UserRole.seller);

    UserView userView = new UserView();
    userView.setId(1);
    userView.setUsername("test");
    userView.setPassword("test");
    userView.setDeposit(new BigDecimal(0));
    userView.setRole(UserRole.seller);

    given(service.updateUser(1, updateUser)).willReturn(userView);
    //given(authenticationController.refreshAuthenticatedUser(any())).methodReturningVoid();

    String uri = "/user/1";
    this.mockMvc.perform(put(uri).contentType(MediaType.APPLICATION_JSON)
            .content(getJsonFromPojo(updateUser)))
        .andDo(print())
        .andExpect(status().isOk());

  }

  @Test
  public void getById_success() throws Exception {
    UserView userView = new UserView();
    userView.setId(1);
    userView.setUsername("test");
    userView.setPassword("test");
    userView.setDeposit(new BigDecimal(0));
    userView.setRole(UserRole.seller);
    given(service.getById(1)).willReturn(userView);

    String uri = "/user/1";
    this.mockMvc.perform(get(uri).contentType(MediaType.APPLICATION_JSON))
        .andDo(print())
        .andExpect(status().isOk());

  }

  @Test
  public void deleteById_success() throws Exception {
    UserView userView = new UserView();
    userView.setId(1);
    userView.setUsername("test");
    userView.setPassword("test");
    userView.setDeposit(new BigDecimal(0));
    userView.setRole(UserRole.seller);
    given(service.getById(1)).willReturn(userView);

    String uri = "/user/1";
    this.mockMvc.perform(delete(uri).contentType(MediaType.APPLICATION_JSON))
        .andDo(print())
        .andExpect(status().isOk());

  }

  private String getJsonFromPojo(Object obj) {
    Gson gson = new Gson();
    return gson.toJson(obj);
  }

}
