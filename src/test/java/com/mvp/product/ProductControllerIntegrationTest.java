package com.mvp.product;

import static org.hamcrest.Matchers.containsString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.mvp.product.business.AddProduct;
import com.mvp.product.business.ProductView;
import java.math.BigDecimal;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest
@AutoConfigureMockMvc
public class ProductControllerIntegrationTest {

  @Autowired
  private MockMvc mockMvc;

  @MockBean
  private ProductService service;

  @Test
  public void addNewProduct_success() throws Exception {
    AddProduct addProduct = new AddProduct();
    addProduct.setSellerId(1);
    addProduct.setProductName("TestProduct");
    addProduct.setCost(new BigDecimal(10));
    addProduct.setAmountAvailable(5);

    ProductView productView = new ProductView();
    productView.setSellerId(1);
    productView.setProductName("TestProduct");
    productView.setCost(new BigDecimal(10));
    productView.setAmountAvailable(5);

    when(service.addProduct(addProduct)).thenReturn(productView);

    this.mockMvc.perform(post("/product"))
        .andDo(print())
        .andExpect(status().isOk())
        .andExpect(content().string(containsString("Hello, Mock")));
  }
}
